using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.Devices.Gateway;
using Newtonsoft.Json;

namespace PrinterModule
{
   public class AzureModule : IGatewayModule
   {
       private string configuration;
       public void Create(Broker broker, byte[] configuration)
       {
           this.configuration = System.Text.Encoding.UTF8.GetString(configuration);
       }

       public void Destroy()
       {
       }

       public  void Receive(Message received_message)
       {
           string recMsg = System.Text.Encoding.UTF8.GetString(received_message.Content, 0, received_message.Content.Length);
           Dictionary<string, string> receivedProperties = received_message.Properties;

           BleConverterData receivedData = JsonConvert.DeserializeObject<BleConverterData>(recMsg);

           Console.WriteLine();
           Console.WriteLine("Module           : [AzureModule]");
           Console.WriteLine("received_message : {0}", recMsg);
           using(var client=new HttpClient())
           {
                var test=client.PostAsync("http://www.vg.no",new StringContent(JsonConvert.SerializeObject(receivedData))).Result;;
                try
                {
                    test.EnsureSuccessStatusCode();         
                }
                catch (System.Exception ex)
                {
                    
                    Console.WriteLine(ex.ToString());
                }
           
           }
           
       }
   }
}