# Testing IOT Edge with dotnetcore #

- Git pull
- Open VSCode
- F5 - View at "Debug console-output"

# Todo: Make it work at linux (should work on windows as is)#
Currently getting errormessages
- "error while loading shared libraries: libgateway.so: cannot open shared object file: No such file or directory"
- "Unable to load dll:  dotnetcore"

## Various tries ##
1. Have tried various load configurations in te gw-config.json file
2. Copied all .so from C:\Users\torangel\.nuget\packages\microsoft.azure.devices.gateway.native.ubuntu.x64\1.1.2\runtimes\ubuntu.16.04-x64\native into the bin folder
3. Published the dotnetcore app on ubuntu instead of just using the bin folder
4. +++



# Resources #

- https://www.linkedin.com/pulse/running-iot-edge-raspbianarm32arm-hf-fai-lai
- https://github.com/Azure/iot-edge/blob/master/core/devdoc/dotnet_core_loader_requirements.md
- https://github.com/Azure/iot-edge/blob/master/core/devdoc/module_loaders.md
- https://docs.microsoft.com/en-us/azure/iot-hub/iot-hub-linux-iot-edge-simulated-device
- https://github.com/Azure/iot-edge/issues/290
- http://blog.faister.com/2017/09/04/building-docker-container-with-azure-iot-edge-cross-compiled-libraries-for-raspberry-pi/
- https://github.com/Azure/iot-edge
- https://github.com/dotnet/core-setup/blob/master/Documentation/design-docs/corehost.md#runtime-resolution